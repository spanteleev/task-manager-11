package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;
import ru.tsc.panteleev.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands."
    );

    public final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close applicaion."
    );

    public final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    public final static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show commands list."
    );

    public final static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show arguments list."
    );

    public final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    public final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    public final static Command TASK_CREATE  = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    public final static Command TASK_REMOVE_BY_ID  = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    public final static Command TASK_REMOVE_BY_INDEX  = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    public final static Command TASK_UPDATE_BY_ID  = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    public final static Command TASK_UPDATE_BY_INDEX  = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    public final static Command TASK_SHOW_BY_ID  = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Display task by id."
    );

    public final static Command TASK_SHOW_BY_INDEX  = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Display task by index."
    );

    public final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list."
    );

    public final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    public final static Command PROJECT_CREATE  = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    public final static Command PROJECT_REMOVE_BY_ID  = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    public final static Command PROJECT_REMOVE_BY_INDEX  = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    public final static Command PROJECT_UPDATE_BY_ID  = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    public final static Command PROJECT_UPDATE_BY_INDEX  = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    public final static Command PROJECT_SHOW_BY_ID  = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Display project by id."
    );

    public final static Command PROJECT_SHOW_BY_INDEX  = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Display project by index."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            COMMANDS, ARGUMENTS,
            PROJECT_LIST, PROJECT_CLEAR, PROJECT_CREATE,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            TASK_LIST, TASK_CLEAR, TASK_CREATE,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
