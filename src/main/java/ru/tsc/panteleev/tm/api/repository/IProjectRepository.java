package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

}
