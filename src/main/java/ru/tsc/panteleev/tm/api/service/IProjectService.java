package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

}
