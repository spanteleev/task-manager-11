package ru.tsc.panteleev.tm.controller;

import ru.tsc.panteleev.tm.api.controller.ICommandController;
import ru.tsc.panteleev.tm.api.service.ICommandService;
import ru.tsc.panteleev.tm.model.Command;
import ru.tsc.panteleev.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println("Welcome to Task Manager");
    }

    @Override
    public void showAbout() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("1.11.0");
    }

    @Override
    public void showHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument);
        }
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (threads): " + availableProcessors);
        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory available: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available: " + NumberUtil.formatBytes(totalMemory));
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showErrorCommand(String command) {
        System.err.printf("Error! This command `%s` not supported... \n", command);
        showHelp();
    }

    @Override
    public void showErrorArgument(String command) {
        System.err.printf("Error! This argument `%s` not supported... \n", command);
        showHelp();
    }

}
